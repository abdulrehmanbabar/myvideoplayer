//
//  DataManager.swift
//  MyVideoPlayer
//
//  Created by Abdul Rehman on 12/6/19.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import UIKit

class DataManager: NSObject {
    
    class func loadVideos() -> NSArray {
       
        
        var videos: [Video] = []
        
        let names = ["Video 1", "Video 2", "Video 3", "Video 4"]
        let urls = ["vid1", "vid2", "vid3", "vid4"]
        let thumbNails = ["th1.png","th2.png","th3.png","th4.png"]
        let descriptions = ["Awsome Entertainment","Very Funny","I cannot stop my laugh","It happens sometimes"]
        
        for count in 0...3
        {
            let _vid  = Video.init(url: urls[count] as NSString, thumbURL: thumbNails[count] as NSString, name: names[count] as NSString, description: descriptions[count] as NSString)            
            videos.append(_vid)
        }
        
        return videos as NSArray
    }

}
